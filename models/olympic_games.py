from tortoise.models import Model
from tortoise import fields


class OlympicGames(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)
    start_date = fields.DatetimeField()
    end_date = fields.DatetimeField()


def __str__(self):
        return self.name

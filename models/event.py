from tortoise.models import Model
from tortoise import fields


class Event(Model):
    id = fields.IntField(pk=True)
    discipline = fields.ForeignKeyField('models.Sport', related_name='events')
    medal = fields.ForeignKeyField('models.Medal', related_name='events', null=True)
    athlete = fields.ForeignKeyField('models.Athlete', related_name='events')
    olympic = fields.ForeignKeyField('models.OlympicGames', related_name='events')

    def __str__(self):
        return f'{self.discipline} - {self.medal} - {self.athlete} - {self.olympic} '

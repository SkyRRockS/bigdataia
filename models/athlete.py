from tortoise.models import Model
from tortoise import fields


class Athlete(Model):
    id = fields.IntField(pk=True)
    full_name = fields.CharField(max_length=255)
    year_birth = fields.IntField(null=True)
    country = fields.ForeignKeyField('models.Country', related_name='athletes')

    def __str__(self):
        return self.full_name

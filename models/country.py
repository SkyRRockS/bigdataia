from tortoise.models import Model
from tortoise import fields


class Country(Model):
    id = fields.IntField(pk=True)
    codeThreeLetters = fields.CharField(max_length=5)
    name = fields.CharField(max_length=50)

    def __str__(self):
        return self.name

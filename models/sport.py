from tortoise.models import Model
from tortoise import fields


class Sport(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=50)


def __str__(self):
    return self.name

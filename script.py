import math

import pandas as pd
from tortoise import run_async, Tortoise

from models.athlete import Athlete
from models.country import Country
from models.event import Event
from models.medal import Medal
from models.olympic_games import OlympicGames
from models.sport import Sport

async def init():
    await Tortoise.init(
        db_url='postgres://postgres:pass123@localhost:5432/postgres',
        modules={'models': ['models.athlete',
                            'models.country',
                            'models.event',
                            'models.medal',
                            'models.olympic_games',
                            'models.sport'
                            ]}
    )
    # Generate the schema
    await Tortoise.generate_schemas()


run_async(init())


async def reset():
    await Event.all().delete()
    await Athlete.all().delete()
    await Country.all().delete()
    await Medal.all().delete()
    await OlympicGames.all().delete()
    await Sport.all().delete()

run_async(reset())

df_medals = pd.read_csv('archive/olympic_medals.csv')
df_athletes = pd.read_csv('archive/olympic_athletes.csv')
df_hosts = pd.read_csv('archive/olympic_hosts.csv')

unique_disciplines = df_medals['discipline_title'].unique()
unique_events = df_medals['slug_game'].unique()
unique_medals = df_medals['medal_type'].unique()
unique_countries = df_medals[['country_name', 'country_3_letter_code']].drop_duplicates()
unique_athletes = df_medals[['athlete_full_name', 'country_3_letter_code']].drop_duplicates()


def convert_to_int(value):
    try:
        if math.isnan(value):
            return 0
        else:
            return int(value)
    except TypeError:
        pass


async def data_fill():
    for discipline in unique_disciplines:
        await Sport.create(name=discipline)

    for olympic in unique_events:
        start_date = df_hosts[df_hosts['game_slug'] == olympic]['game_start_date'].values[0]
        end_date = df_hosts[df_hosts['game_slug'] == olympic]['game_end_date'].values[0]
        await OlympicGames.create(name=olympic, start_date=start_date, end_date=end_date)

    for medal in unique_medals:
        await Medal.create(name=medal)

    for index, row in unique_countries.iterrows():
        name = row['country_name']
        code = row['country_3_letter_code']
        await Country.create(name=name, code=code)

    for index, row in unique_athletes.iterrows():
        full_name = row['athlete_full_name']
        country = await Country.get(code=row['country_3_letter_code'])

        year_birth = df_athletes[df_athletes['athlete_full_name'] == full_name]['athlete_year_birth']
        year_birth = convert_to_int(year_birth)

        await Athlete.create(full_name=full_name, country=country, year_birth=year_birth)

    for index, row in df_medals.iterrows():
        discipline = await Sport.get(name=row['discipline_title'])
        event = await Event.get(name=row['slug_game'])
        medal = await Medal.get(name=row['medal_type'])
        athlete = await Athlete.get(full_name=row['athlete_full_name'], country__code=row['country_3_letter_code'])
        await Event.create(discipline=discipline, event=event, medal=medal, athlete=athlete)

run_async(data_fill())

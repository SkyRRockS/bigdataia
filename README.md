# BigDataIa

Notre objectif est de prédire le nombre de médailles de chaque pays au jeux olypmpiques de Paris 2024.

```
Récupérer des datasets sur Kaggle et d’autres sites si besoin
2. A l’aide d’un notebook ou d’une webapp, analyser les jeux de données récupérés :
○ Combien d’athlètes français ont été médaillés dans l’histoire des JO ?
○ Quels sont les plus grands athlètes français dans l’histoire des JO ?
○ Comment se classe la France dans l’histoire des JO ?
○ Quels sont les pays qui ont le plus gagné par discipline ?
○ Quels sont les athlètes qui ont le plus gagné de médailles dans l’histoire des JO ?
3. Pouvez-vous prédire les pays médaillés dans au moins 2 disciplines lors des JO de Paris ?
4. Pouvez-vous prédire les athlètes médaillés dans au moins 2 disciplines lors des JO de Paris ?
5. Pouvez-vous prédire le nombre total de médailles françaises lors des JO de Paris ?
6. Que pensez-vous de cet article ?
7. Quelles conclusions tirez-vous de votre étude des JO par les données ? Pouvez-vous parier les yeux
fermés avec vos résultats ?
8. Avez-vous besoin du Big Data pour ce projet ? Justifiez votre réponse 
```

## Dataset

Dans un premier temps nous récupérons un dataset. Le dataset que nous avons choisi contient pour chaque année et pays le nombre de médailles.

Ce dataset est disponible sur Kaggle:

https://www.kaggle.com/datasets/piterfm/olympic-games-medals-19862018


## MCD

![alt text](mcd/mcd.png)

## Article

Dans cet article nous pouvons apprendre des choses intéressantes dont notament que les methodes utilisées pour les prédictions n'ont pas énormement évoluées. En effet, la random forest était déjà la plus utilisées et elle l'est encore aujourd'hui.

## Bigdata

Le BigData peut être intéressant mais le problème c'est que nous n'avons pas assez de données pour l'exploiter. En effet, si nous voulions un projet complet, sur plusieurs annees et avec beaucoup de moyens peut-être que le bigdata aurait plus d'intêret.
Mais dans notre cas, nous manquons clairement de données, il faudrait croiser les données de chaqun des sports, les formatter pour pouvoir les utiliser et enfin entrainer notre modèle avec des centaine de milliers de données.
De plus si nous nous interressions un peu a chaqun des sports meme en ne regardant que quelques competitions, nous pourrions deja voir une tendance globale sans avoir besoin de l'ia.
Prenons plusieurs examples:
Dans le biathlon, on peut se rendre assez vite compte qu'il y a une grande nation du biathlon (la Norvège) et trois nations qui se battent souvent pour les places secondaires (la France, la Suisse et l'Allemagne), on pourrait donc assez simplement supposer que la tendance ne changerait pas pour les jo.
Prenons un autre exemple avec un sport par equipe.
Si nous prenons le basket, on se doute avec l'impact de la nba et les competitions précédentes que les USA et la France feraient parti des favoris et finiraient tres probablement médaillé.

## Prediction

Vous pouvez retrouver mon modèle de prédiction dans le fichier prediction.py
```python
def random_forest_classifier_model(x_train, y_train, x_test, y_test):
    model = RandomForestClassifier()
    model.fit(x_train, y_train)
    predicted = model.predict(x_test)
    conf = confusion_matrix(y_test, predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf, display_labels=model.classes_)
    disp.plot()
    plt.title("Random Forest")
    plt.show()
    print("The Accuracy is: ", accuracy_score(y_test, predicted) * 100)
```

Legrand Yanis,
Hollande Youn

